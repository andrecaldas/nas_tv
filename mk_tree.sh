#! /bin/bash

set -x
set -e

SCRIPT_PATH="$(dirname "$(realpath "$0")")"
. "$SCRIPT_PATH/script_vars"

#
# Multistrap
#

mkdir -p deb_files_cache
rm -rf "$TREE_ROOT"
mkdir -p "$TREE_ROOT"
$FAKEROOT "$MULTISTRAP" --no-auth -d "$TREE_ROOT" -f "$MULTISTRAP_CONF" --tidy-up


#
# Make initrd.img
#
# Could not find a nice way to cross generate a initramfs. :-(
#
mkdir -p "$TREE_ROOT/boot"
rm -f "$TREE_ROOT/boot/vmlinuz"
ln -s "/boot/vmlinuz-$(ls "$TREE_ROOT/lib/modules/")" "$TREE_ROOT/boot/vmlinuz"
cp "$SCRIPT_PATH/modules" "$TREE_ROOT/etc/initramfs-tools/"
# RESUME=none avoids trying to recover from "suspend to disk".
# Setting it here works only because (now) RESUME is not set in initramfs.conf.
RESUME=none /usr/sbin/mkinitramfs -o "$TREE_ROOT/boot/initrd.img" -d "$TREE_ROOT/etc/initramfs-tools/"


# Move init away.
[ -f "$TREE_ROOT/sbin/init.orig" ] || mv "$TREE_ROOT/sbin/init" "$TREE_ROOT/sbin/init.orig"

# Install EFI Grub.
mkdir -p "`dirname "$EFI_FULL_PATH"`"
$GRUB_EFI_MAKER -O x86_64-efi -o "$EFI_FULL_PATH" --modules="$GRUB_MODULES" boot/grub/grub.cfg="grub/bootstrap.cfg"

echo "Imagem para NAS TV criada com sucesso!"
