#! /bin/bash

set -x
set -e

SCRIPT_PATH="$(dirname "$(realpath "$0")")"
. "$SCRIPT_PATH/script_vars"

if [ ! -f "$SIMULATOR_HDA" ]; then
  qemu-img create -f qcow2 "$SIMULATOR_HDA" 200G
fi

if [ ! -f "$SIMULATOR_HDB" ]; then
  qemu-img create -f qcow2 "$SIMULATOR_HDB" 200G
fi

qemu-system-x86_64 -vga cirrus -m 1G -bios /usr/share/qemu/OVMF.fd "$IMG_FILE" \
  -hdb "$SIMULATOR_HDA" -hdc "$SIMULATOR_HDB" #\
#  -net nic -net tap,ifname=tap0,script=no,downscript=no

