#!/bin/sh

SCRIPT_PATH="$(dirname "$_")"

set -x
set -e

. "$SCRIPT_PATH/kerberos_vars.inc"

#
# Kerberos server principal.
#

kadmin.local -q 'add_principal -randkey nfs/nastv@$REALM'


#
# Certificate to mount.
#

kadmin.local -q 'add_principal -nokey nfs@$REALM'


#
# Generate /etc/exports.
#

if grep 'BEGIN NAS EXPORTS' /etc/exports; then true; else
  # TODO: substitute template vars with command line stuff.
  cat /usr/local/etc/exports.template >> /etc/exports
fi

exportfs -ra


#
# Generate NFS mounter certificate and key.
# We do not want to protect this key,
# because we want everyone to being able to mount.
#

kadmin.local -q 'add_principal -nokey WELLKNOWN/ANONYMOUS@NASTV'
#openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -out "$NFS_MOUNTER_KEY"
#openssl req -config "$NFS_MOUNTER_CONFIG" -new -out "$NFS_MOUNTER_REQ" -key "$NFS_MOUNTER_KEY" -verbose
#openssl x509 -req -extfile "$NFS_MOUNTER_CONFIG" \
#             -in "$NFS_MOUNTER_REQ" -out "$NFS_MOUNTER_CERTIFICATE" \
#             -CA "$CA_CERTIFICATE" -CAkey "$CA_KEY" \
#             -CAcreateserial
#rm "$KDC_REQ"

#cat "$NFS_MOUNTER_CERTIFICATE" "$NFS_MOUNTER_KEY" > "$NFS_MOUNTER_CERTIFICATE_AND_KEY"

