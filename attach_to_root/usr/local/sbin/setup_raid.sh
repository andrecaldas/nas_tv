#!/bin/bash

exit 1
set -x
set -e

#
# Detect HDs to use.
#
function device_var ()
{
  local device="$1"
  local varname="$2"
  lsblk -npbdo "$varname" "$device"
}

declare -a HD_DEVICES
for device in $(lsblk -npdo NAME); do
  TYPE="$(device_var "$device" TYPE)"
  SIZE="$(device_var "$device" SIZE)"
  MOUNTPOINT="$(device_var "$device" MOUNTPOINT)"
  STATE="$(device_var "$device" STATE)"
  if [ ! "$TYPE" == 'disk' ]; then continue; fi
  if [ ! "$MOUNTPOINT" == '' ]; then continue; fi
  if [ ! "$STATE" == 'running' ]; then continue; fi
  if test "$SIZE" -gt 100000000000; then
    HD_DEVICES+=("$device")
  fi
done


#
# Make raid device.
#

RAID_DEV=/dev/md/nas-raid
/sbin/sgdisk "${HD_DEVICES[0]}" --zap-all
/sbin/sgdisk "${HD_DEVICES[1]}" --zap-all
yes | mdadm --create --verbose "$RAID_DEV" \
	--level=raid1 \
	--raid-devices=2 \
	"${HD_DEVICES[0]}" \
	"${HD_DEVICES[1]}"
udevadm trigger --action=add -s block
udevadm settle


#
# Format if not already in use.
#

# Aproximate calculus to take 80% of disk for the first partition.
/sbin/sgdisk "$RAID_DEV" --zap-all
/sbin/sgdisk "$RAID_DEV" --sort # This is just for the output... without it, next command fails.
/sbin/sgdisk "$RAID_DEV" --new 1:0:"$(expr '(' $(/sbin/sgdisk --end-of-largest "$RAID_DEV") '*' 80 ')' / 100)"
/sbin/sgdisk "$RAID_DEV" --new 2:0:+10G
/sbin/sgdisk "$RAID_DEV" --new 3:0:0
/sbin/sgdisk "$RAID_DEV" --change-name 1:hd_plain
/sbin/sgdisk "$RAID_DEV" --typecode 2:8200
/sbin/sgdisk "$RAID_DEV" --change-name 2:swap_crypt
/sbin/sgdisk "$RAID_DEV" --change-name 3:hd_crypt
find /dev/nastv || true
partprobe
find /dev/nastv || true
udevadm settle
find /dev/nastv || true

#
# Enable encrypted swap.
#

sed -Ee 's,^#@HD_swap@[ ]*,,' -i /etc/crypttab
cryptdisks_start cswap

sed -Ee 's,^#@HD_swap@[ ]*,,' -i /etc/fstab
swapon -a
free # REMOVE... just to check if swap is on.


#
# Enable new filesystems.
#

yes | mkfs.xfs -f -L public /dev/nastv/by-partlabel/hd_plain #"$RAID_DEV"1
#yes | mkfs.xfs -f -L private /dev/nastv/by-partlabel/hd_crypt #"$RAID_DEV"3
udevadm settle

sed -Ee 's,^#@HD@[ ]*,,' -i /etc/crypttab
sed -Ee 's,^#@HD@[ ]*,,' -i /etc/fstab


#
# Mount.
#

mkdir -p /mnt/nas_public
mount LABEL=public


#
# Create directories and permissions.
#
chmod 1777 /mnt/nas_public
#mkdir -p /mnt/nas_public/pictures

