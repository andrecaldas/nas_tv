#!/bin/bash
#
# Create user and setup authorized ssh public keys.
#

SCRIPT_PATH="$(dirname "$_")"

set -x
set -e

function usage () {
  echo "Usage: $0 <login>."
  exit 1
}

export login="$1"
if [ '' == "$login" ]; then
  echo "No login provided."
  usage
fi
if id "$login" > /dev/null 2>&1; then
  echo 'User "$login" already exists!' 1>&2
  exit 1
fi


#
# Add user to unix auth.
#

adduser --disabled-password --gecos '' "$login"


#
# Attach input to authorized_keys
#
mkdir -p -m 700 /home/$login/.ssh
echo 'Paste your public key (.ssh/id_rsa.pub) and then <enter> Ctrl-D.'
echo 'Or Ctrl-C to quit...'
cat >> /home/$login/.ssh/authorized_keys
chown $login: /home/$login/.ssh /home/$login/.ssh/authorized_keys
