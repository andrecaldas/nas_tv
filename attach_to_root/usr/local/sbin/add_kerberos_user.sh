#!/bin/bash
#
# Create user and generate client certificate.
#

SCRIPT_PATH="$(dirname "$_")"

set -x
set -e

. "$SCRIPT_PATH/kerberos_vars.inc"

function usage () {
  echo "Usage: $0 <login> <client certificate request file>."
  exit 1
}

export CLIENT="$1"
if [ '' == "$CLIENT" ]; then
  echo "No login provided."
  usage
fi
if echo "$CLIENT" | grep "'"; then
  echo "Client name cannot contain <'>."
fi
if id "$CLIENT" > /dev/null 2>&1;
  echo 'Client "$CLIENT" already exists!' 1>&2
  exit 1
fi

CLIENT_REQ="$2"
if [ '' == "$CLIENT_REQ" ]; then
  echo "You need to specify the <request file> (e.g.: client_req.pem)."
  usage
fi

#
# Add user to unix auth.
#

useradd --no-create-home "$CLIENT"

#
# Add user principal.
#

kadmin.local <<EOF
  add_principal +requires_preauth -nokey 'nfs/${CLIENT}@$REALM'
  add_principal +requires_preauth -nokey '${CLIENT}@$REALM'
EOF


#
# Generate key and certificate_request in client computer.
# $ openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -out client_key.pem
# $ openssl req -new -out certificate_request.pem -key client_key.pem


# Sign client certificate.
openssl x509 -req -extfile "$CLIENT_CONFIG" \
             -in "$CLIENT_REQ" \
             -CA "$CA_CERTIFICATE" -CAkey "$CA_KEY"


# kadmin.local...

