#!/bin/bash

SCRIPT_PATH="$(dirname "$_")"

set -x
set -e

. "$SCRIPT_PATH/kerberos_vars.inc"

#
# Generate CA and KDC certificates.
#

KDC_REQ="/tmp/kdc.req"


mkdir -p "$CA_BASE_DIR" \
         "$CA_CERTIFICATES_DIR"
mkdir -p -m 700 "$CA_PRIVATE_DIR"


# CA key and certificate.
#read "Password for creation of new users and certificate signature: " pass
openssl req -config "$CA_CONFIG" -new -x509 -nodes -keyout "$CA_KEY" -out "$CA_CERTIFICATE" -verbose

# KDC certificate.
openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -out "$KDC_KEY"
openssl req -config "$KDC_CONFIG" -new -out "$KDC_REQ" -key "$KDC_KEY" -verbose
openssl x509 -req -extfile "$KDC_CONFIG" \
             -in "$KDC_REQ" -out "$KDC_CERTIFICATE" \
             -CA "$CA_CERTIFICATE" -CAkey "$CA_KEY" \
             -CAcreateserial
rm "$KDC_REQ"

