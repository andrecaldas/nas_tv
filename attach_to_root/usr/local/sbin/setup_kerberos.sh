#!/bin/bash

SCRIPT_PATH="$(dirname "$_")"

set -x
set -e

. "$SCRIPT_PATH/kerberos_vars.inc"

#
# Create REALM database.
#
password="$RANDOM$RANDOM$RANDOM$RANDOM"
(krb5_newrealm || true) <<EOF
${password}
${password}
EOF


#
# Set up KDC to use certificates.
#

KDC_CONF="/etc/krb5kdc/kdc.conf"
if grep -E '^\[[[:space:]]*kdcdefaults[[:space:]]*\]' "$KDC_CONF"; then true; else
  echo -e "\n\n[kdcdefaults]" >> "$KDC_CONF"
fi

if grep pkinit_identity "$KDC_CONF"; then
  sed -i -E -e 's|^([[:space:]]*)pkinit_identity([[:space:]]*)=.*$|\1pkinit_identity\2= '"FILE:$KDC_CERTIFICATE,$KDC_KEY|" "$KDC_CONF"
else
  sed -i -E -e 's|^([[:space:]]*\[kdcdefaults\][[:space:]]*)$|\1\n    pkinit_identity = '"FILE:$KDC_CERTIFICATE,$KDC_KEY|" "$KDC_CONF"
fi

if grep pkinit_anchors "$KDC_CONF"; then
  sed -i -E -e 's|^([[:space:]]*)pkinit_anchors([[:space:]]*)=.*$|\1pkinit_anchors\2= '"FILE:$CA_CERTIFICATE|" "$KDC_CONF"
else
  sed -i -E -e 's|^([[:space:]]*\[kdcdefaults\][[:space:]]*)$|\1\n    pkinit_anchors = '"FILE:$CA_CERTIFICATE|" "$KDC_CONF"
fi

systemctl restart krb5-kdc.service

