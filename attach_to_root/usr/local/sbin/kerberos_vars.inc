#!/bin/bash

set -x
set -e

#
# Generate CA and KDC certificate.
#

CA_BASE_DIR="/usr/local/etc/kerberos/ca"
CA_PRIVATE_DIR="$CA_BASE_DIR/private"
CA_CERTIFICATES_DIR="$CA_BASE_DIR/certs"
CA_KEY="$CA_PRIVATE_DIR/ca_key.pem"
CA_CERTIFICATE="$CA_CERTIFICATES_DIR/ca_certificate.pem"

KDC_KEY="$CA_PRIVATE_DIR/kdc_key.pem"
KDC_CERTIFICATE="$CA_CERTIFICATES_DIR/kdc_certificate.pem"

# Environment used in $CA_CONFIG.
export REALM="$(echo get krb5-config/default_realm | debconf-communicate | cut -d ' ' -f 2)"

# Configure certificate parameters.
CA_CONFIG="$CA_BASE_DIR/ca.conf"
KDC_CONFIG="$CA_BASE_DIR/kdc.conf"
CLIENT_CONFIG="$CA_BASE_DIR/client.conf"


#
# NFS Mounter Certificate.
# Since we want everyone to compute, this key is not protected.
#

NFS_MOUNTER_CERTIFICATE_AND_KEY="$CA_BASE_DIR/nfs_mounter_key_and_certificate.pem"
NFS_MOUNTER_KEY="$CA_BASE_DIR/nfs_mounter_key.pem"
NFS_MOUNTER_CERTIFICATE="$CA_BASE_DIR/nfs_mounter_certificate.pem"
NFS_MOUNTER_CONFIG=""

