#! /bin/bash

set -x
set -e

SCRIPT_PATH="$(dirname "$(realpath "$0")")"
. "$SCRIPT_PATH/script_vars"

export TMPDIR=$PWD/tmp/
mkdir -p "$TMPDIR"

#
# Make disk image
#

if [ ! -f "$IMG_FILE" ]; then
  ROM_DISK_GUID="nas_tv_disk" # Useless. By now, we use lsblk -pno pkname
  dd if=/dev/zero of="$IMG_FILE" bs=1M count=2000
  $SGDISK "$IMG_FILE" --disk-guid "$ROM_DISK_GUID"
  $SGDISK "$IMG_FILE" --new 1:0:+30M     # /boot/efi
  $SGDISK "$IMG_FILE" --new 2:0:0        # /
  $SGDISK "$IMG_FILE" --typecode=1:0xef00
fi

$FAKE_SHELL -a "$IMG_FILE" run \
  : mkfs vfat /dev/sda1 label:"$EFI_PARTITION_LABEL" \
  : mkfs ext4 /dev/sda2 label:"$ROOT_PARTITION_LABEL" \
  : mount /dev/sda2 / \
  : mkdir-p /boot/efi

$FAKEROOT tar -C "$TREE_ROOT" -c . |
  $FAKE_SHELL -a "$IMG_FILE" -m /dev/sda2 -m /dev/sda1:/boot/efi -- tar-in - / xattrs:true : df-h

$FAKEROOT tar -C "attach_to_root/" -c . |
  $FAKE_SHELL -a "$IMG_FILE" -m /dev/sda2 -m /dev/sda1:/boot/efi -- tar-in - / xattrs:true : df-h

